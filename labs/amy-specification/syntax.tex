\section{Syntax}
\label{sec:syntax}
\def\alt{~~|~~}

\def\Expr{\gnt{Expr}}
\def\Id{\gnt{Id}}
\def\ID{\gnt{ID}}
\def\({\gt{(}}
\def\){\gt{)}}

\begin{figure}
\begin{equation*}
\begin{array}{rl}
\gnt{Program} \gpo & \gnt{Module^*} \\
\gnt{Module} \gpo & \gt{object} \Id  \gnt{Definition^*} \gnt{Expr?} \gt{end} \Id \\
\gnt{Definition} \gpo & \gnt{AbstractClassDef} \alt \gnt{CaseClassDef} \alt \gnt{FunDef} \\
\gnt{AbstractClassDef} \gpo & \gt{abstract} \gt{class} \Id \\
\gnt{CaseClassDef} \gpo & \gt{case} \gt{class} \Id \( \gnt{Params} \) \gt{extends} \Id \\
\gnt{FunDef} \gpo & \gt{fn} \gnt{Id} \( \gnt{Params} \) \gt{:} \gnt{Type} \gt{=} \gt{\{} \Expr \gt{\}} \\
    \gnt{Params} \gpo & \ \, \epsilon \alt \gnt{ParamDef} [\gt{,} \gnt{ParamDef}]^* \\
\gnt{ParamDef} \gpo & \gnt{Id} \gt{:} \gnt{Type} \\
\gnt{Type} \gpo & \gt{Int} \( \gnt{32} \) \alt \gt{String} \alt \gt{Boolean} \alt \gt{Unit} \alt [\Id \gnt{.}]? \Id \\
\gnt{Expr}  \gpo & \Id \\
            \alt & \gnt{Literal} \\
            \alt & \Expr\ \ \gnt{BinOp}\ \ \Expr    \\
            \alt & \gnt{UnaryOp} \Expr \\
            \alt & \; [\Id \gnt{.}]? \Id \( \gnt{Args} \) \\
            \alt & \Expr \gt{;} \Expr   \\
            \alt & \gt{val} \gnt{ParamDef} \gt{=} \gnt{Expr} \gt{;} \gnt{Expr} \\
            \alt & \gt{if} \( \Expr \) \gt{\{} \gnt{Expr} \gt{\}} \gt{else} \gt{\{} \gnt{Expr} \gt{\}} \\
            \alt & \Expr \gt{match} \gt{\{} \gnt{MatchCase^+} \gt{\}} \\
            \alt & \gt{error} \gt{(} \Expr \gt{)} \\
            \alt & \( \Expr \)    \\
\gnt{Literal} \gpo & \gt{true} \alt \gt{false} \alt \( \) \\
              \alt & \gnt{IntLiteral} \alt \gnt{StringLiteral} \\
\gnt{BinOp} \gpo & \gt{+}\alt \gt{-} \alt \gt{*} \alt \gt{/} \alt \gt{\%} \alt \gt{<} \alt \gt{<=} \\
            \alt & \gt{\&\&} \alt \gt{||} \alt \gt{==} \alt \gt{++} \\
\gnt{UnaryOp} \gpo & \gt{-} \alt \gt{!} \\
\gnt{MatchCase} \gpo & \gt{case} \gnt{Pattern} \gt{=>} \Expr \\
\gnt{Pattern} \gpo & \; [\Id \gnt{.}]? \Id \( \gnt{Patterns} \) \alt \Id \alt \gnt{Literal} \alt \gt{\_} \\
    \gnt{Patterns} \gpo & \ \; \epsilon \alt \gnt{Pattern} [\gt{,} \gnt{Pattern}]^* \\
    \gnt{Args} \gpo & \ \, \epsilon \alt \Expr [\gt{,} \Expr]^* \\
\end{array}
\end{equation*}
\caption{Syntax of \langname}
\label{figure:syntax}
\end{figure}

\begin{figure}
\begin{equation*}
\begin{array}{rl}
\gnt{IntLiteral} \gpo & \gnt{Digit^+}\\
\gnt{Id} \gpo & \gnt{Alpha} \gnt{AlphaNum^*} \text{(and not a reserved word)}\\
\gnt{AlphaNum} \gpo & \gnt{Alpha} \alt \gnt{Digit}\ \alt \gtns{\_} \\
\gnt{Alpha} \gpo & ~[\gtns{a}-\gtns{z}]\ \alt [\gtns{A}-\gtns{Z}] \\
\gnt{Digit} \gpo & ~[\gtns{0}-\gtns{9}] \\
\gnt{StringLiteral} \gpo & ~\gtns{"} \gnt{StringChar^*} \gtns{"}\\
\gnt{StringChar} \gpo & ~\text{Any character except newline and $\gtns{"}$}
\end{array}
\end{equation*}
\caption{Lexical rules for \langname}
\label{figure:lexing}
\end{figure}


The syntax of \langname is given formally by the context-free grammar of Figure~\ref{figure:syntax}.
Everything spelled in $italic$ is a nonterminal symbol of the grammar,
whereas the terminal symbols are spelled in \gtns{monospace} font.
$^*$ is the Kleene star, $s^+$ stands for one or more repetitions of $s$,
and $?$ stands for optional presence of a symbol (zero or one repetitions).
The square brackets $[]$ are not symbols of the grammar,
they merely group symbols together.

Before parsing an \langname program, the Amy \emph{lexer} generates a sequence of terminal symbols
(\emph{tokens}) from the source files. 
Some non-terminal symbols mentioned, but not specified, in Figure~\ref{figure:syntax}
are also represented as a single token by the lexer.
They are lexed according to the rules in Figure~\ref{figure:lexing}.
In Figure~\ref{figure:lexing}, we denote the range between characters $\alpha$ and $\beta$ (included)
with $[\alpha - \beta]$.

The syntax in Figure~\ref{figure:syntax} is an \emph{overapproximation} of the real syntax of \langname.
This means that it allows some programs that should not be allowed in Amy.
To get the real syntax of Amy, there are some additional restrictions presented (among other things)
in the following notes:

\begin{itemize}
    \item The reserved words of \langname are the following: 
        \gtns{abstract}, \gtns{Boolean}, \gtns{case}, \gtns{class}, \gtns{fn},
        \gtns{else}, \gtns{error}, \gtns{extends}, \gtns{false}, \gtns{if}, \gtns{Int}, \gtns{match},
        \gtns{object},
        \gtns{end},
        \gtns{String}, \gtns{true}, \gtns{Unit}, \gtns{val}, \gtns{\_} (the wildcard pattern).

        Identifiers are not allowed to coincide with a reserved word.
    \item The operators and language constructs of \langname 
        have the following precedence, starting from the \emph{lowest}:
    
        (1) \lstinline{val}, \lstinline{;}
        (2) \lstinline{if}, \lstinline{match} (3) \lstinline{||}
        (4) \lstinline{&&} (5) \lstinline{==}
        (6) \lstinline{<}, \lstinline{<=} (7) \lstinline{+}, \lstinline{-}, \lstinline{++}
        (8) \lstinline{*}, \lstinline{/}, \lstinline{%}
        (9) Unary \lstinline{-}, \lstinline{!}
        (10) \lstinline{error}, calls, variables, literals, parenthesized expressions.

        For example,\\
        \lstinline{1 + 2 * 3} means \lstinline{1 + (2 * 3)} and \\
        \lstinline|1 + 2 match {...}| means \lstinline|(1 + 2) match {...}|.

        A little more complicated is the interaction between \lstinline{;} and \lstinline{val}:
        the definition part of the \lstinline{val} extends only as little as the first semicolon,
        but then the variable defined is visible through any number of semicolons.
        Thus
        \lstinline{(val x: Int(32) = y; z; x)} means \lstinline{(val x: Int(32) = y; (z; x)) }
        \sloppy{and not \lstinline{(val x: Int(32) = (y; z); x)} or \lstinline{((val x: Int(32) = y; z); x)}}
        (i.e. \lstinline{x} takes the value of y and is visible until the end of the expression).

        All operators are left-associative. That means that within
        the same precedence category, the leftmost application of an operator takes precedence.
        An exception is the sequence operator, which for ease of the implementation
        (you will understand during parsing)
        can be considered right-associative (it is an associative operator so it does not really
        matter).
    \item A \lstinline{val} definition is not allowed directly in the value assigned
        by an enclosing \lstinline{val} definition.
        E.g. \lstinline{(val x: Int(32) = val y: Int(32) = 0; 1; 2)} is not allowed.
        On the other hand, \lstinline{(val x: Int(32) = 0; val y: Int(32) = 1; 2)} is allowed.
    \item It is not allowed to use a \lstinline{val} as a (second) operand to an operator.
        E.g. \lstinline{(1 + val x: Int(32) = 2; x)} is not allowed.
    \item A unary operator is not allowed as a direct argument of another unary operator.
        E.g. \lstinline{--x} is not allowed.
    \item It is not allowed to use \lstinline{match} as a first operand of any binary operator,
        except \lstinline{;}. E.g. \lstinline|(x match { ... } + 1)| is not allowed.
        On the other hand \lstinline|(x match { ... }; x)| is allowed.
    \item The syntax $[\Id \gnt{.}]? \Id$ refers to an optionally qualified name,
        for example either \lstinline{MyModule.foo} or \lstinline{foo}.
        If the qualifier is included, the qualified name refers to a definition
        \lstinline{foo} in another module \lstinline{MyModule};
        otherwise, \lstinline{foo} should be defined in the current module.
        Since \langname does not have the import statement of Scala or Java,
        this is the only way to refer to definitions in other modules.
	\item One line comments are introduced with ``\lstinline{//}'': \lstinline{// This is a comment}.
        Everything until the end of the line is a comment and should be ignored by the lexer.
    \item Multiline comments can be used as follows: \lstinline{/* This is a comment */}.
        Everything between the delimiters is a comment, notably including newline characters
        and \lstinline{/*}. Nested comments are not allowed.
    \item Escaped characters are not recognised inside string literals.
        I.e. \lstinline{"\n"} stands for a string literal which contains
        a backspace and an ``n''.
\end{itemize}

