# EPFL CS320 - Computer language processing, 2022

[Course Description and Schedule](https://edu.epfl.ch/coursebook/en/computer-language-processing-CS-320)

Lectures: 
  * Mondays 13:15-15:00 in [INM 200](https://drive.google.com/file/d/1JuHdXpaxju9xCqdrCI3GCufdUovb0UrO/view)

Labs or Exercises and First Lecture:
  * Wednesday 8:15-10:00 in [INM 202](https://drive.google.com/file/d/1-4bCuIp3R3L6dfMGBLNd8juvK0Wmwh8p/view)
  * Thursday 8:15-10:00 in [INM 202](https://drive.google.com/file/d/1-4bCuIp3R3L6dfMGBLNd8juvK0Wmwh8p/view)

## Grading

The grade is based on a midterm (30%) as well as project work (70%) that has many aspects: the implementation in [Scala](https://www.scala-lang.org/), a written report, oral presentation, and answers to questions, in presentation and throughout the semester. There will be no written exam. Here is the weight of the milestones in the overall course grade:

  * 30% Midterm on Monday 2022-11-14, 13:00-15:00 (see [the archive of past exams](past-exams/))
  * 10% Lab 1
  * 10% Lab 2
  * 10% Lab 3
  * 10% Lab 4
  * 10% Lab 5
  * 20% Lab 6 (Compiler extension, customized)

For the final Lab 6, each group will need to present the project in a slot in one of the last two weeks of the semester. The final report on the project will need to handed in after the end of the semester, by 2023-01-09 but the students are encouraged to complete it during the semester.

## Staff

| Role            | People |
| :---            | :--- |
| Professors      | [Viktor Kunčak](https://people.epfl.ch/viktor.kuncak), [Self-Introduction](https://tube.switch.ch/videos/82f0eb14) |
| PhD (Head) TA   | [Fatih Yazici](https://people.epfl.ch/fatih.yazici?lang=en) |
| Initial Autograding Setup | [Rodrigo Raya](https://people.epfl.ch/rodrigo.raya/) and [Matthieu Bovel](https://people.epfl.ch/matthieu.bovel) |
| MSc TA | [Valentin Aebi](https://people.epfl.ch/valentin.aebi) |
| MSc TA | [Romain Birling](https://people.epfl.ch/Romain.Birling) |
| MSc TA | [Solène Husseini](https://people.epfl.ch/tanguy.husseini) |
| MSc TA | [Ana-Maria Indreias](https://people.epfl.ch/ana-maria.indreias) |
| MSc TA | Benoît Léo Maillard |
| MSc TA | [Victor Arthur Schneuwly](https://people.epfl.ch/victor.schneuwly) |
| MSc TA | [Erwan Serandour](https://people.epfl.ch/erwan.serandour) |
| MSc TA | [Abel Vexina Wilkinson](https://people.epfl.ch/abel.vexinawilkinson) |

## Books

My goal is for lectures to be self-contained. The following books contain overlapping material with some recommendations for most relevant parts:
  * [Basics of Compiler Design](http://hjemmesider.diku.dk/~torbenm/Basics/). Read pages 9-88 (omit Section 2.8) for lexical analysis and parsing
  * [Modern compiler implementation in ML](http://library.epfl.ch/en/beast?isbn=9781107266391). Read Sections 2.1-2.4 for Lexical analysis, Sections 3.1-3.2 for parsing, and 5.3-5.4 as well as 16.1-16.3 for type checking
  * [Compilers, principle, techniques and tools](http://library.epfl.ch/en/beast?isbn=9781292024349)

## Schedule and Material

| Week | Day | Date       | Time  | Room   | Topic                | Videos & Slides              |                              |
| :--  | :-- | :--        | :--   | :--    | :--                  | :--                          | :--                          |
| 1    | Wed | 21.09.2022 | 08:15 | INM202 | Lecture 1            | [Intro to CLP](https://tube.switch.ch/videos/3351ec99), [Formal languages](https://tube.switch.ch/videos/6df3a6ba) |
|      | Thu | 22.09.2022 | 08:15 | INM202 | Lecture 2            | [Operations on Languages](https://tube.switch.ch/videos/1846c6f2), [Regular Expressions and Lexer Idea](https://tube.switch.ch/videos/c903b9d5) |
| 2    | Mon | 26.09.2022 | 13:15 | INM200 | Lecture 3            | [First Symbols. Constructing a Lexer](https://tube.switch.ch/videos/fc864ce0), [From Regular Expressions to Automata](https://tube.switch.ch/videos/a15b1c5f) |
|      | Wed | 28.09.2022 | 08:15 | INM202 | Labs 1 | [Interpreter Lab](labs/lab01/)  |
|      | Thu | 29.09.2022 | 08:15 | INM202 | Exercises 1 | [Exercises on regular expressions and automata](exercises/ex1/) |
| 3    | Mon | 03.10.2022 | 13:15 | INM200 | Lecture 4            | [Introduction to Grammars](https://tube.switch.ch/videos/ef9ae6c1), [Syntax Trees](https://tube.switch.ch/videos/9314ac16) |
|      | Wed | 05.10.2022 | 08:15 | INM202 | Labs  | [Interpreter Lab](labs/lab01/), [Lexer Lab](labs/lab02/)  |
|      | Thu | 06.10.2022 | 08:15 | INM202 | Exercises  | [Exercises on lexical analyzers and basics of grammars](exercises/ex2/) |
| 4    | Mon | 10.10.2022 | 13:15 | INM200 | Lecture 5            | [LL(1) Parsing](https://tube.switch.ch/videos/38dd46b4) |
|      | Wed | 12.10.2022 | 08:15 | INM202 | [Lexer Lab](labs/lab02/), [Parser Lab](labs/lab03/), [Parser Lab Presentation](https://tube.switch.ch/videos/TUALwnHh6u). See also [Scallion Presentation](https://tube.switch.ch/videos/f18a2692) |
|      | Thu | 13.10.2022 | 08:15 | INM202 | Exercises  |  |
| 5    | Mon | 17.10.2022 | 13:15 | INM200 | Lecture 6            | [CYK Algorithm for Parsing General Context-Free Grammars](https://tube.switch.ch/videos/672add06) and [Chomsky Normal Form Transformation](https://tube.switch.ch/videos/2d3503f4) |
| 6    | Mon | 24.10.2022 | 13:15 | INM200 | Lecture 7            | [Name Analysis](https://tube.switch.ch/videos/a842b90d), [Inductive Relations](https://tube.switch.ch/videos/5d67c147), [Operational Semantics](https://tube.switch.ch/videos/465af7b1) |
| 7    | Mon | 31.10.2022 | 13:15 | INM200 | Lecture 8            | [Type Rules, Progress, Preservation](https://tube.switch.ch/videos/bdb5c902), [Type Inference](https://tube.switch.ch/videos/14facfc5) |
| 8    | Mon | 07.11.2022 | 13:15 | INM200 | Lecture 9            | Finish [Type Inference](https://tube.switch.ch/videos/14facfc5) and start code generation |
| 9    | Mon | 14.11.2022 | **13:00** | [INM 200](https://plan.epfl.ch/?room==INM%20200) + [SG0211](https://plan.epfl.ch/?room==SG%200211) | **MIDTERM** | Materials of lectures 1-9 |
|      | Wed | 12.10.2022 | 08:15 | INM202 | Labs  |
|      | Thu | 13.10.2022 | 08:15 | INM202 | Midterm Solution Presentation |  |
